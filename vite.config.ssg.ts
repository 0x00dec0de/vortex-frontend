import {defineConfig} from 'vite';

import {plugins} from './.builder';
import {sharedConfig} from './vite.config.shared';

/**
 * SSG development config
 */
export default defineConfig({
	...sharedConfig,
	plugins: [
		...(sharedConfig.plugins ?? []),
		plugins.viteHtmlEntryReplacer('./index-ssg.html'),
	],
});
