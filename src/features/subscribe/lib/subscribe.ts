import { defineStore } from 'pinia'
import { ref } from 'vue'

// todo real subscription
export const useSubscriptionStore = defineStore('subscriptionStore', () => {
	const subscribed = ref(false);

	const toggleSubscription = () => {
		console.log('toggle subs')
		subscribed.value = !subscribed.value;
	}

	return { subscribed, toggleSubscription };
});

