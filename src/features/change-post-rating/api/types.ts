export enum PostVoteValue {
	PLUS = 1,
	MINUS = -1
}