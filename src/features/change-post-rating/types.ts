import type {PostPublished} from '@/entities/post';

export type ChangePostRatingProps = {
	post: PostPublished;
};

export type ChangePostRatingEmits = {
	(e: 'show-details'): void;
};

export type PostRatingProps = {
	post: PostPublished;
};
