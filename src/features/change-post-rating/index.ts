import PostRating from './PostRating.vue';
import NotAuthorizedModal from './ui/NotAuthorizedModal.vue';

export type {PostVoteValue} from './api/types';

export { NotAuthorizedModal, PostRating as ChangePostRating };
