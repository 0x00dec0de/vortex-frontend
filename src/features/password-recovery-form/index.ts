import PasswordConfirmForm from './ui/PasswordConfirmForm.vue';
import PasswordRecoveryDone from './ui/PasswordRecoveryDone.vue';
import PasswordRecoveryForm from './ui/PasswordRecoveryForm.vue';

export { PasswordConfirmForm, PasswordRecoveryDone, PasswordRecoveryForm };
