export type PasswordRecoveryFormEmits = {
    (e: 'submit'): void;
    (e: 'change-email'): void;
};
