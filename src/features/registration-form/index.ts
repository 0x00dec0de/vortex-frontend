import type { RegistrationFormEmits } from './types';
import AccountVerificationForm from './ui/AccountVerificationForm.vue';
import RegistrationForm from './ui/RegistrationForm.vue';

export { RegistrationForm, AccountVerificationForm };
export type { RegistrationFormEmits };
