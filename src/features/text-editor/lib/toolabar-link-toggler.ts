import type {Editor} from "@tiptap/vue-3";

export const setLink = (editor: Editor | undefined) => {
	if (!editor) {
		return;
	}

	const previousUrl = editor.getAttributes('link').href
	const url = window.prompt('URL', previousUrl)

	// cancelled
	if (url === null) {
		return
	}

	// empty
	if (url === '') {
		editor
			.chain()
			.focus()
			.extendMarkRange('link')
			.unsetLink()
			.run()

		return
	}

	if (!/^https?:\/\//.test(url)) {
		return;
	}

	// update link
	editor
		.chain()
		.focus()
		.extendMarkRange('link')
		.toggleLink({ href: url })
		.run()
}
