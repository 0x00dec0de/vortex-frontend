import { defineStore } from 'pinia';
import { ref } from 'vue';

import { feeds } from '../consts';

export const useFeedStore = defineStore('feed-switch-store', () => {
	const activeFeed = ref('popular');

	const changeFeed = (feed: string) => {
		if (feeds[feed]) {
			activeFeed.value = feed;
		}
	}

	return { activeFeed, changeFeed };
});
