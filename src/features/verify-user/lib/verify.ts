import { defineStore } from 'pinia'
import { ref } from 'vue'

// todo real verification
export const useVerificationStore = defineStore('verification-store', () => {
	const verified = ref(false);

	const confirmVerification = () => {
		verified.value = true;
	}

	return { verified, confirmVerification };
});

