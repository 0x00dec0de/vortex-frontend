import {ref} from 'vue';

import {useMobileNavigationStore, useMobileProfileNavigationStore} from '@/shared/providers/stores';

const MobileNavigationStore = useMobileNavigationStore();
const MobileProfileNavStore = useMobileProfileNavigationStore();

//TODO shared layer should not importing from outside the mobile-navigation-sidebar
// move or refactor

//TODO sidebar is not entity

export enum ESidebars {
	NAVIGATION = 'navigation',
	PROFILE = 'profile',
}

const sidebars = ref({
	navSidebar: MobileNavigationStore.showNavigationPage,
	profileSidebar: MobileProfileNavStore.showProfileNavigationPage,
	toggleSidebar: (sidebar?: ESidebars) => {
		switch (sidebar) {
			case ESidebars.NAVIGATION:
				sidebars.value.setNavSidebar(true);
				sidebars.value.setProfileSidebar(false);
				return;
			case ESidebars.PROFILE:
				sidebars.value.setNavSidebar(false);
				sidebars.value.setProfileSidebar(true);
				return;
			default:
				sidebars.value.setNavSidebar(false);
				sidebars.value.setProfileSidebar(false);
		}
	},
	setNavSidebar: (val: boolean) => {
		if ((MobileNavigationStore.showNavigationPage && !val) || val) {
			MobileNavigationStore.toggleNavigationPage();
			sidebars.value.navSidebar = MobileNavigationStore.showNavigationPage;
		}
	},
	setProfileSidebar: (val: boolean) => {
		if ((MobileProfileNavStore.showProfileNavigationPage && !val) || val) {
			MobileProfileNavStore.toggleProfileNavigationPage();
			sidebars.value.profileSidebar = MobileProfileNavStore.showProfileNavigationPage;
		}
	},
});

export { sidebars };
