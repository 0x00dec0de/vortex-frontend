import InfinityScroll from './InfinityScroll.vue';
import {useViewBlocks} from './lib/use-view-blocks';
import type {InfinityScrollProps, InfinityScrollEmits} from './types';

export { useViewBlocks, InfinityScroll };

export type {
	InfinityScrollProps,
	InfinityScrollEmits
};
