export type InfinityScrollProps<T> = {
	list: T[],
};

export type InfinityScrollEmits = {
	(e: 'scroll-start'): void;
	(e: 'scroll-end'): void;
};
