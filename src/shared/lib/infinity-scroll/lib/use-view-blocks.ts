import type {VNodeRef, Ref} from "vue";
import {ref} from "vue";

//todo move types to posts-feed-store
type BLock<T> = T[];
type List<T> = BLock<T>[];

export const useViewBlocks = <T> (list: T[]) => {
    const blocks = ref<List<T>>([]) as Ref<List<T>>;

    const topBlockIndex = ref<number>(0);
    const topViewBlockIndex = ref<number>(1);
    const bottomViewBlockIndex = ref<number>(2);
    const bottomBlockIndex = ref<number>(3);

    const topBlock = ref<VNodeRef | undefined>(undefined);
    const topViewBlock = ref<VNodeRef | undefined>(undefined);
    const bottomViewBlock = ref<VNodeRef | undefined>(undefined);
    const bottomBlock = ref<VNodeRef | undefined>(undefined);

    const setup = (list: BLock<T>) => {
        let block: BLock<T> = [];
        const newBlocks: List<T> = [];

        for (const item of list) {
            if (block.length < 2) {
                block.push(item);
            } else {
                newBlocks.push(block);
                block = [];
            }
        }

		if (block.length > 0) {
			newBlocks.push(block);
		}

        blocks.value = newBlocks;
    }

    const moveUp = () => {
        if (topBlockIndex.value === 0) {
            return;
        }

        topBlockIndex.value--;
        topViewBlockIndex.value--;
        bottomViewBlockIndex.value--;
        bottomBlockIndex.value--;
    };

    const moveDown = () => {
        if (bottomBlockIndex.value === blocks.value.length) {
            return;
        }

        topBlockIndex.value++;
        topViewBlockIndex.value++;
        bottomViewBlockIndex.value++;
        bottomBlockIndex.value++;
    }

    const isTopBlock = (index: number) => index === topBlockIndex.value;
    const isTopViewBlock = (index: number) => index === topViewBlockIndex.value;
    const isBottomViewBlock = (index: number) => index === bottomViewBlockIndex.value;
    const isBottomBlock = (index: number) => index === bottomBlockIndex.value;

    if (list.length) {
        setup(list);
    }

    return {
        blocks,
        topBlockIndex, topViewBlockIndex, bottomViewBlockIndex, bottomBlockIndex,
        topBlock, topViewBlock, bottomViewBlock, bottomBlock,
        setup, moveUp, moveDown,
        isTopBlock, isTopViewBlock, isBottomViewBlock, isBottomBlock
    };
}
