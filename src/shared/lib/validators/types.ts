export type ConditionResult = {
	error: boolean,
	text: string
}

export type ConditionChecker = (value: string) => ConditionResult;
