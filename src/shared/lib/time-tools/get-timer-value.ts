export const getTimerValue = (countdown: number) => {
	const date = new Date(1970, 0, 1);
	date.setSeconds(countdown);

	const hours = date.getHours() > 9 ? date.getHours() : '0' + date.getHours();
	const minutes = date.getMinutes() > 9 ? date.getMinutes() : '0' + date.getMinutes();
	const seconds = date.getSeconds() > 9 ? date.getSeconds() : '0' + date.getSeconds();

	return `${hours}:${minutes}:${seconds}`;
}
