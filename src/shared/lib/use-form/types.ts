import type {ConditionChecker, ConditionResult} from "@/shared/lib/validators";

export type FieldSetupProps = {
	value: string,
	validators: ConditionChecker[],
}

export type FormSetupProps = {
	[key: string]: FieldSetupProps,
}

export type Fields = {
	[key: string]: string,
}

export type Validators = {
	[key: string]: ConditionChecker[],
}

export type FieldErrors = ConditionResult[];

export type Errors = {
	[key: string]: FieldErrors
}
