import {reactive, watch, ref} from "vue";

import type {Fields, Validators, Errors, FormSetupProps, FieldSetupProps} from "./types";

export const useForm = (props: any) => {
	const fields = reactive<Fields>({});
	const validators: Validators = {};
	const errors = reactive<Errors>({});

	const hasErrors = ref(false);

	const addField = (name: string, props: FieldSetupProps) => {
		fields[name] = props.value;
		validators[name] = props.validators;
		errors[name] = [];

		watch(() => fields[name], () => {
			validate(name);
		});
	};

	const findErrors = () => {
		for (const name in errors) {
			for (const error of errors[name]) {
				if (error.error) {
					return true;
				}
			}
		}

		return false;
	};

	const validate = (name: string) => {
		const field = fields[name];

		errors[name] = [];

		for (const validator of validators[name]) {
			const error = validator(field);
			errors[name].push(error);
		}

		hasErrors.value = findErrors();
	};

	const validateAll = () => {
		for (const name in fields) {
			validate(name);
		}
	};

	const setup = (props: FormSetupProps) => {
		for (const name in props) {
			addField(name, props[name]);
		}
	};

	if (props) {
		setup(props);
	}

	return { fields, errors, hasErrors, validateAll };
};
