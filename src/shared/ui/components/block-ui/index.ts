import BlockWindowUI from './blocks/BlockWindowUI.vue';
import ElongatedBlockUI from './blocks/ElongatedBlockUI.vue';
import ModalBlockUI from './blocks/ModalBlockUI.vue';
import TwoColumnsBlock from './blocks/TwoColumnsBlock.vue';
import UniformBlockUI from './blocks/UniformBlockUI.vue';
import CentralColumn from './columns/CentralColumn.vue';
import DesktopAsideColumnUI from './columns/DesktopAsideColumnUI.vue';
import DesktopCentralColumnUI from './columns/DesktopCentralColumnUI.vue';
import StickyPanelUI from './panels/StickyPanelUI.vue';

export {
	BlockWindowUI,
	ElongatedBlockUI,
	UniformBlockUI,
	CentralColumn,
	DesktopAsideColumnUI,
	DesktopCentralColumnUI,
	StickyPanelUI,
	TwoColumnsBlock,
	ModalBlockUI,
};
