export type BlockWindowUIEmits = {
    (e: 'close'): void;
};
