export type SuspenseUIProps = {
	loading: boolean,
	error?: string | null,
}
