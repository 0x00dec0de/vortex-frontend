export interface IButton {
	stretched?: boolean;
	borderRadius?: number;
	isLoading?: boolean;
	disabled?: boolean;
}
