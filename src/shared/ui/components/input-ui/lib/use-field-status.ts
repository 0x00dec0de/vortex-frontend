import {ref} from 'vue';

import {FieldStatuses} from '../types';

export const useFieldStatus = () => {
	const status = ref<FieldStatuses>();
	const setFieldStatus = (newStatus: FieldStatuses) => {
		status.value = newStatus;
	};

	return { status, setFieldStatus };
};
