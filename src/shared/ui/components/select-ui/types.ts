export type Option = {
	text: string;
	value: string | null;
	selectable?: boolean,
};

export type Props = {
	options: Option[];
	modelValue: Option | null;
	attractorPlaceholder: string;
};

export type Emits = {
	(e: 'update:modelValue', value: Option): void;
};
