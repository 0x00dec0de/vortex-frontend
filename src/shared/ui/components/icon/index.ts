import Icon from './Icon.vue';
import type {IconProps} from './types';
import {IconSizes} from './types';

export { Icon, IconSizes };

export type { IconProps };
