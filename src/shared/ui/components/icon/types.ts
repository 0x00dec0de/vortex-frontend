export enum IconSizes {
	EIGHT = 'eight',
    TEN = 'ten',
    TWELVE = 'twelve',
    SIXTEEN = 'sixteen',
    EIGHTEEN = 'eighteen',
    TWENTY_FOUR = 'twentyFour',
    THIRTY_TWO = 'thirtyTwo',
    SIXTY_FOUR = 'sixtyFour',
    NINETY_SIX = 'ninetySix',
}

export type IconProps = {
	size: IconSizes | IconSizes.SIXTEEN
    clickable?: boolean | false,
};
