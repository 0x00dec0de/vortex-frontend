import {useMobileNavigationStore} from './mobile-navigation-store';
import {useMobileProfileNavigationStore} from './mobile-profile-navigation-store';

export { useMobileNavigationStore, useMobileProfileNavigationStore };
