export type ApiTokens = {
    accessToken: string,
    refreshToken: string,
}

type ErrorsDetail = {
    type: string,
    msg: string,
};

type ErrorsDetails = ErrorsDetail[];

export type ServiceAPIErrorResponse = {
    code: number,
    detail: ErrorsDetails | string,
}

export type APIError = {
    code: number | null,
    message?: string,
    messages?: string[]
}
