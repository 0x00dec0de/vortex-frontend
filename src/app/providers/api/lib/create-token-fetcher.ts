import {HTTPMethods} from "@/shared/api";

import type {ApiTokens} from '../types';

export const createTokenFetcher = (url: string) => {
	return async (refreshToken: string, accessToken: string): Promise<ApiTokens> => {
		const response: Response = await fetch(url, {
			method: HTTPMethods.POST,
			body: JSON.stringify({ refresh_token: refreshToken }),
			headers: {
				'Accept': '*/*',
				'Content-Type': 'application/json',
				'Authorization': `Bearer ${accessToken}`
			}
		});

		if (response.ok) {
			const tokens = await response.json();

			return {
				accessToken: tokens.access_token,
				refreshToken: tokens.refresh_token,
			};
		} else {
			throw new Error('Failed to update tokens' + (response.statusText ? `: ${response.statusText}` : ''));
		}
	};
}
