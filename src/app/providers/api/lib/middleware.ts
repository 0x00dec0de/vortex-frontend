import type {Middleware} from "@/shared/api";

import {getAPIError} from "../lib/get-api-error";
import {refreshTokens} from "../model/session-local-storage";

export const middleware: Middleware = async (request) => {
    try {
        return await request();
    } catch (error: any) {
        if (error.code !== 401) {
            throw getAPIError(error);
        }
    }

    try {
        await refreshTokens();
        return await request();
    } catch (error: any) {
        throw getAPIError(error);
    }
};
