import {setAPI, createApi} from "@/shared/api";
import {APIEndpoints, APINames} from "@/shared/config";

import {middleware} from "./lib/middleware";
import {getAccessToken} from "./model/session-local-storage";

export const createAPI = () => {
    setAPI({
        [APINames.AUTH]: createApi({
            endpoint: APIEndpoints[APINames.AUTH],
            getAccessToken,
            middleware,
        }),
        [APINames.MONOLITH]: createApi({
            endpoint: APIEndpoints[APINames.MONOLITH],
            getAccessToken,
            middleware,
        })
    });
};
