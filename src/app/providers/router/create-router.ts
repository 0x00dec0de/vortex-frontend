import {
	createRouter as _createRouter,
	createMemoryHistory,
	createWebHistory,
	type Router,
	type RouterOptions,
	type RouteRecordRaw,
} from 'vue-router';

/**
 * Shared router between SPA, SSR, SSG
 */
export function createRouter(routes: Record<string, RouteRecordRaw>, options?: RouterOptions): Router {
	// Different history for client and server
	// import.meta.env.SSR is injected by Vite
	const isServer = import.meta.env.SSR;
	const history = isServer
		? createMemoryHistory()
		: createWebHistory();

	const flatRoutes = Object.entries(routes).map(([name, route]) => ({name, ...route}));

	const router = _createRouter({
		history,
		routes: flatRoutes,
		...options,
	});

	// Change title on page change
	router.beforeEach((to, from, next) => {
		if (!isServer) {
			document.title = String(to.meta.title);
		}

		next();
	});

	return router;
}
