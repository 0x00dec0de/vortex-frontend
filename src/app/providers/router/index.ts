import type {RouteMeta} from 'vue-router';

export * from './create-router';

export const meta = (meta: string | RouteMeta): RouteMeta => {
	return typeof meta === 'string' ? {title: meta} : meta;
};
