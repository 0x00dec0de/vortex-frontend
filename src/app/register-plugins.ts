import type {App} from 'vue';

import {outsideClickDirective} from './providers/directives';

/**
 * Register directives and other extra plugins
 */
export function registerPlugins(app: App<Element>): void {
	app.directive('outside-click', outsideClickDirective);
}
