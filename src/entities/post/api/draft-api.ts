
import {getAPI} from "@/shared/api";
import {APINames} from "@/shared/config";

import type {ICorePost, PostDraft} from "../model/types";

export const savePost = async (post: ICorePost) => {
	return await getAPI(APINames.MONOLITH).post<PostDraft>('/posts/', {
		...post
	});
};

export const publishPost = async (slug: string) => {
	await getAPI(APINames.MONOLITH).post(`/posts/${slug}/publish/`);
};
