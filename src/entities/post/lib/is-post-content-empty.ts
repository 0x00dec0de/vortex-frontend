import {PostContentTypes} from "../model/types";
import type {PostContent} from "../model/types";

export const isEmpty = (content: PostContent) => {
    if (!content || content.length === 0)
        return true;

    const block = content[0];
    const type = block.type;

    switch (type) {
        case PostContentTypes.HTML:
            return !block.value;
        case PostContentTypes.IMAGE:
            return !block.src;
    }

    return false;
}
