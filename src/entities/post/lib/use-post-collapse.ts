import { ref } from "vue";

export const usePostCollapse = () => {
	const minCollapsableHeight = 700;
	const minHiddenHeight = 300;

	const collapsable = ref(false);
	const collapsed = ref(false);

	const post = ref<HTMLElement | null>(null);

	const toggleCollapsed = () => {
		collapsed.value = !collapsed.value;

		if (collapsed.value) {
			post.value?.scrollIntoView({ behavior: "instant", block: "nearest", inline: "nearest" });
		}
	}

	const setup = (postElement: HTMLElement | null) => {
		if (postElement == null) {
			return;
		}

		post.value = postElement;

		const isCollapsable = (
			(post.value.offsetHeight > minCollapsableHeight) &&
			(post.value.offsetHeight - minCollapsableHeight) > minHiddenHeight
		);

		collapsable.value = isCollapsable;
		collapsed.value = isCollapsable;
	};

	return {
		collapsed, collapsable, post,
		toggleCollapsed, setup,
	}
}
