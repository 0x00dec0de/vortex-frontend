import type {ICorePost} from "../model/types";

export const createPost = (): ICorePost => {
    return {
        title: null,
        content: [],
        tags: [],
    };
};
