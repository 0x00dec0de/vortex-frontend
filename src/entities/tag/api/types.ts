import type {Tag} from "../model/types";

export type FindTagResponse = {
    name: Tag,
    usages: number,
}[];
