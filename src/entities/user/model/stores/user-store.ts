import { defineStore } from 'pinia';
import { ref } from 'vue';

// eslint-disable-next-line
import type { APIError } from '@/app/providers/api';

import { getUser } from '../../api/viewer-api';
import type { UserProfile } from '../../model/types';

export const useUserStore = defineStore('user-store', () => {
	const profile = ref<UserProfile | null>(null);
	const isLoading = ref(false);
	const error = ref<APIError | null>(null);

	const setUser = (newProfile: UserProfile) => {
		profile.value = newProfile;
	};

	const loadUser = async (username: string) => {
		isLoading.value = true;
		error.value = null;

		try {
			const profile = await getUser(username);
			setUser(profile);
		} catch (err: any) {
			error.value = err;
		} finally {
			isLoading.value = false;
		}
	};

	return {
		profile,
		isLoading,
		error,
		loadUser,
	}
});
