export enum AuthStatues {
	NOT_LOADED = 'NOT_LOADED',
	PENDING = 'PENDING',
	NOT_AUTHORIZED = 'NOT_AUTHORIZED',
	AUTHORIZED = 'PENDING',
}

export type ShortUserProfile = {
    username: string;
    avatar: string;
}

export interface UserProfile extends ShortUserProfile {
	external_user_uid: string,
	email: string,
	date_of_birth: Date,
	bio: string,
	rating: number,
	comments_count: number,
	votes_up_count: number,
	votes_down_count: number
}

export enum EPostActionsDirection {
	NORMAL = 'normal',
	INVERTED = 'inverted'
}

export interface IUserSettings {
	postActionsDirection: EPostActionsDirection;
}
