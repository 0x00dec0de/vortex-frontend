import {getAPI} from "@/shared/api";
import {APINames} from "@/shared/config";

import type { UserProfile } from '../model/types';

export const getUser = async (name: string): Promise<UserProfile> => {
	return await getAPI(APINames.MONOLITH).get<UserProfile>(`/users/${name}/`);
};
