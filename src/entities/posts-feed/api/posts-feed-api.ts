import {getAPI} from "@/shared/api";
import {APINames} from "@/shared/config";

import {responseToPostsPublished, responseToPostsDraft, argsToGetPostsParams} from "../lib/posts-mapper";
import type {
	PostsPublishedResponse,
	RawPostsPublishedResponse,
	PostsDraftResponse,
	RawPostsDraftResponse, IGetPostsParams
} from '../types';

export const getPublishedPosts = async (args?: IGetPostsParams): Promise<PostsPublishedResponse> => {
	const response: RawPostsPublishedResponse = await getAPI(APINames.MONOLITH)
			.getWithCredentials(`/posts/`, argsToGetPostsParams(args));

	return {
		...response,
		results: responseToPostsPublished(response.results),
	};
};

export const getDraftPosts = async (args: IGetPostsParams): Promise<PostsDraftResponse> => {
	const response: RawPostsDraftResponse = await getAPI(APINames.MONOLITH)
		.getWithCredentials(`/posts/`, argsToGetPostsParams(args));

	return {
		...response,
		results: responseToPostsDraft(response.results),
	};
};
