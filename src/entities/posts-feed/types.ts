// eslint-disable-next-line
import type {PostPublished, PostDraft} from '@/entities/post';

export type Fetcher = (cursor: string | null) => Promise<PostsPublishedResponse>;

export interface BasePostsResponse {
    count: number;
    next: string;
    previous: string;
    results: any;
}

export interface IGetPostsParams {
    cursor?: string | null;
    username?: string | null;
    period?: string | null;
    tag?: string | null;
}


export type RawPostsDraftResponseResults = {
    tags: { name: string, usages: number }[]
} & PostDraft;
export interface RawPostsDraftResponse extends BasePostsResponse {
    results: RawPostsDraftResponseResults[],
}
export interface PostsDraftResponse extends BasePostsResponse {
    results: PostDraft[],
}


export type RawPostsPublishedResponseResults = {
    tags: { name: string, usages: number }[]
} & PostPublished;
export interface RawPostsPublishedResponse extends BasePostsResponse {
    results: RawPostsPublishedResponseResults[],
}
export interface PostsPublishedResponse extends BasePostsResponse {
    results: PostPublished[],
}
