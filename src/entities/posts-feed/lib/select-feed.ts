import {PostsFeeds} from "../config";
import {useNewFeed, useUserFeed} from "../model/posts-feeds-stores";

export const selectPublishedFeed = (name: PostsFeeds) => {
	switch (name) {
		case PostsFeeds.NEW: return useNewFeed();
		case PostsFeeds.USER: return useUserFeed();
	}
}
