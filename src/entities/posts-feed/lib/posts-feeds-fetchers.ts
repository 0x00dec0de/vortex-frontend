import {getPublishedPosts} from "../api/posts-feed-api";

export const createDraftFeedFetcher = (username: string) => {
	return async (cursor: string | null) => {
		return getPublishedPosts({
			username,
			cursor,
		});
	}
}

export const createPublishedFeedFetcher = () => {
    return async (cursor: string | null) => {
        return getPublishedPosts({
            cursor,
        });
    }
}

export const createUserFeedFetcher = (username: string) => {
    return async (cursor: string | null) => {
        return getPublishedPosts({
            username,
            cursor,
        });
    }
}


