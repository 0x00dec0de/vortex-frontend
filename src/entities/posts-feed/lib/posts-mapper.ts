// eslint-disable-next-line
import type {PostPublished, PostDraft} from "@/entities/post";
import {getTimeAgo} from "@/shared/lib/time-tools";

import type {RawPostsPublishedResponseResults, RawPostsDraftResponseResults} from "../types";

//todo
export const argsToGetPostsParams = (args: any): Record<string, string> => {
    const params: any = {};

    Object.keys(args).forEach((key: string) => {
        if (args[key]) {
            params[key] = args[key];
        }
    })

    return params as Record<string, string>;
}

export const responseToPostsPublished = (response: RawPostsPublishedResponseResults[]): PostPublished[] => {
    return response.map(post => {
        const publishedAtDate = new Date(post.published_at);

        return {
            ...post,
            tags: post.tags.map((tag) => tag.name),
            published_at: publishedAtDate,
            time_ago: getTimeAgo(publishedAtDate),
        };
    });
}

export const responseToPostsDraft = (response: RawPostsDraftResponseResults[]): PostDraft[] => {
    return response.map(post => ({
            ...post,
            tags: post.tags.map((tag) => tag.name),
    }));
};

