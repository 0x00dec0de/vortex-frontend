import {defineStore} from "pinia";
import {ref, type Ref} from "vue";
//TODO
// eslint-disable-next-line
import {PostStatusEnum} from "@/entities/post";

//TODO
// eslint-disable-next-line
import type {PostDraft, PostPublished} from "@/entities/post";
import {useRequest} from "@/shared/api";
import {DEFAULT_ERROR_MESSAGE} from "@/shared/config";

import type {PostsPublishedResponse, PostsDraftResponse, BasePostsResponse, Fetcher} from "../types";

const createFeedStore = <Post, PostsResponse extends BasePostsResponse>(name: string) => {
	return defineStore(`${name}-feed-store`, () => {
		const {request, isLoading, error} = useRequest(DEFAULT_ERROR_MESSAGE);
		const posts = ref<Post[]>([]) as Ref<Post[]>;
		const isLastPage = ref(false);

		let cursor: string | null = null;

		const set = (newPosts: Post[]) => {
			posts.value = newPosts;
		};

		const add = (newPosts: Post[]) => {
			posts.value = [...posts.value, ...newPosts];
		};

		const upload = async (fetcher: Fetcher) => {
			if (isLastPage.value)
				return;

			const response = await request<PostsResponse>(() => fetcher(cursor));

			if (!response) {
				return;
			}

			if (response.results) {
				add(response.results);
			}

			cursor = response.next;
			isLastPage.value = !response.next;
		}

		const reload = async (fetcher: Fetcher) => {
			set([]);
			isLastPage.value = false;
			cursor = null;
			await upload(fetcher);
		}

		return {
			posts, isLastPage,
			isLoading, error,
			upload, reload
		};
	});
};

export const useDraftFeed =
	createFeedStore<PostDraft, PostsDraftResponse>(PostStatusEnum.DRAFT);
export const useNewFeed =
	createFeedStore<PostPublished, PostsPublishedResponse>(PostStatusEnum.PUBLISHED);
export const useUserFeed =
	createFeedStore<PostPublished, PostsPublishedResponse>(PostStatusEnum.PUBLISHED);
