export enum PostsFeeds {
    TRENDS = 'TRENDS',
    NEW = 'NEW',
    TOP = 'TOP',
    DISCUSSED = 'DISCUSSED',
    SUBSCRIPTIONS = 'SUBSCRIPTIONS',
    COPYRIGHT = 'COPYRIGHT',
    GROUPS = 'GROUPS',
    TAGS = 'TAGS',
	DRAFT = 'DRAFT',
	USER = 'USER',
}
