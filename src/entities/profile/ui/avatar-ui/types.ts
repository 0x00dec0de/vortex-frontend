export interface IAvatarProps {
	size: 16 | 32 | 64 | 100 | 128;
	round: boolean;
	src: string;
	borderRadius?: number;
	fromAssets?: boolean;
}
