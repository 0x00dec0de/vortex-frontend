import ProfileHeader from "./ui/ProfileHeader.vue";
import ProfileInformation from "./ui/ProfileInformation.vue";
import ProfileStats from "./ui/ProfileStats.vue";

export {ProfileHeader, ProfileInformation, ProfileStats};
export {defaultStats} from './model/consts';

export type {IProfileStatsProps, IProfileStats, IProfileInformation, IProfileHeader} from './model/types';
