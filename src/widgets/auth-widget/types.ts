export enum AUTH_FORMS {
	NONE = 'none',
	LOGIN = 'login',
	REGISTRATION = 'registration',
	PASSWORD_RECOVERY = 'password-recovery',
	PASSWORD_CONFIRM = 'password-confirm',
	PASSWORD_RECOVERY_DONE = 'password-recovery-done',
	ACCOUNT_VERIFICATION_FORM = 'account-verification-form',
}
