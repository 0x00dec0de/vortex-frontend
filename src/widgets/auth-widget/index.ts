import AuthWidget from './AuthWidget.vue';

export type {AUTH_FORMS} from './types'
export {
    AuthWidget,
}
