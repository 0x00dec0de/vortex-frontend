import SidebarNavigationMobileWidget from './SidebarNavigationMobileWidget.vue';

export type { SidebarNavigationMobileWidgetProps, INavLink } from './types';
export {
    SidebarNavigationMobileWidget
}
