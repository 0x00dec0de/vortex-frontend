import {join} from 'node:path';

import {build, type InlineConfig} from 'vite';

import {SsrRenderer, Fs} from './.builder';
import {sharedConfig} from './vite.config.shared';

/**
 * SSG production build
 */

process.env.VITE_HYDRATE = 'true';

const {root} = Fs;
const entryClient = join(root, '/index-ssg.html');
const entryServer = join(root, '/@/main-ssg-server.ts');
const routesPath = join(root, '/@/pages/ssg');
const distPath = join(root, '/dist-ssg');
const ssrOutDir = join(distPath, '/.server');

const configs: InlineConfig[] = [
	// SPA
	{
		...sharedConfig,
		configFile: false,
		build: {
			...(sharedConfig.build ?? {}),
			outDir: distPath,
			ssrManifest: true,
			rollupOptions: {
				input: entryClient,
			},
		},
	},
	// SSR
	{
		...sharedConfig,
		configFile: false,
		build: {
			...(sharedConfig.build ?? {}),
			outDir: ssrOutDir,
			ssr: true,
			rollupOptions: {
				input: entryServer,
			},
		},
	}
];

(async () => {
	// Vite build SPA & SSR
	for (const config of configs) {
		await build(config);
	}

	// Transform to SSG
	await SsrRenderer.transformToSsg({
		entryClient,
		entryServer,
		distPath,
		ssrOutDir,
		routesPath,
		rootPage: 'MainPage.vue',
	});
})();
