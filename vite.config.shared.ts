import {fileURLToPath, URL} from 'node:url';

import vue from '@vitejs/plugin-vue';
import {defineConfig} from 'vite';
import svgLoader from 'vite-svg-loader';

import {Fs, plugins} from './.builder';

/**
 * Shared config between SPA & SSR & SSG
 */

export const MODE = process.env.NODE_ENV ?? 'development';
export const IS_DEV = MODE === 'development';
export const IS_PROD = MODE === 'production';

export const sharedConfig = defineConfig({
	mode: MODE,// production | development
	root: Fs.root,
	publicDir: 'public',
	// server: {
	// 	proxy: {
	// 		'https://backend.kapi.bar/v1/': {
	// 			target: 'https://backend.kapi.bar',
	// 			changeOrigin: true,
	// 			secure: false,
	// 		},
	// 		'https://auth.kapi.bar/v1': {
	// 			target: 'https://backend.kapi.bar',
	// 			changeOrigin: true,
	// 			secure: false,
	// 		}
	// 	}
	// },
	build: {
		minify: IS_PROD,// default "esbuild"
		cssMinify: IS_PROD,// default "esbuild"
		sourcemap: IS_PROD ? false : 'inline',
		emptyOutDir: true,
		copyPublicDir: true,
	},
	resolve: {
		alias: {
			'@': fileURLToPath(new URL('./src', import.meta.url)),
			'~': fileURLToPath(new URL('./src', import.meta.url)),
		}
	},
	// TODO: move styles to index, remove this
	css: {
		preprocessorOptions: {
			scss: {
				additionalData: `@use "./src/shared/ui/styles/config/index.scss" as *;`,
			},
		},
	},
	plugins: [
		vue(),
		svgLoader(),
		plugins.viteHtmlMinifier({
			collapseWhitespace: IS_PROD,
		}),
	],
});
