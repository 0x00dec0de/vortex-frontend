import {readFile} from 'node:fs/promises';
import type {PluginOption, Plugin} from 'vite';

/**
 * Replace "index.html" entry with another ".html" entry
 */
export const viteHtmlEntryReplacer = (path: string): Plugin | PluginOption => {
	let cache: string;

	return {
		name: 'vite-html-entry-replacer',
		transformIndexHtml: {
			enforce: 'pre',
			async transform() {
				return cache ?? (cache = await readFile(path, 'utf8'));
			},
		},
	}
};
